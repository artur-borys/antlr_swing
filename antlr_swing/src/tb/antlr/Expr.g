grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | block)+ EOF!;
    
block :
    EB (stat | block)* LB // dzięki alternatywie, bloki kodu można zagnieżdżać
    ;

stat
    : expr NL -> expr
    // deklaracja zmiennej i opcjonalne podstawienie wartości
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    // podstawienie wartości do zmiennej
    | ID PODST expr NL -> ^(PODST ID expr)
    // wyświetlenie wyrażenia
    | PRINT expr NL -> ^(PRINT expr)
    // ignorowanie znaku(ów) nowej linii
    | NL ->
    ;
    
expr
    : asExpr
      ( SHL^ asExpr
      | SHR^ asExpr
      )*
    ;

asExpr // add or subtract expression
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      // w większości języków programowania operator modulo jest na równi z * i / w kolejności
      | MODULO ^ powExpr
      )*
    ;
// operacja potęgowania ma pierwszeństwo przed powyższymi operacjami
powExpr
    : atom
      ( POW^ powExpr)? // prawostronna łączność
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

// { oraz } są przekazywane do wyniku, tak aby w gramatyce drzewa można było określić dla nich akcje
EB // enter block
  : '{'
  ;

LB // leave block
  : '}'
  ;

VAR :'var';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ;
  
MODULO
  : '%'
  ;

SHL
  : '<<'
  ;

SHR
  : '>>'
  ;