package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	// tablica symboli lokalnych (stos tablic symboli dla bloków kodu)
	protected LocalSymbols locals = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    // wyświetlenie wyrażenia i jego wartości na strumieniu wyjściowym
    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    // wczytanie integera ze stringa
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	// wyświetlenie komunikatu o błędzie dotyczącym symbolu, wraz z wyświetleniem numeru linii, w której błąd wystąpił
	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	/* ******** operacje arytmetyczne ***********/
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer div(Integer a, Integer b) {
		if(b == 0) {
			throw new RuntimeException("You cannot divide by 0");
		}
		return a/b;
	}
	
	// metoda, która podnosi liczbę a do potęgi b. Wersja rekurencyjna
	protected Integer pow(Integer a, Integer b) {
		if( b < 0 ) return 0; // dla b < 0 a^b należy do (0, 1) - nie można przedstawić jako int
		else if( b == 0 ) return 1; // a^0 = 1
		else if( b == 1 ) return a; // a^1 = a
		else if( b % 2 == 0 ) return pow(a*a, b/2); // dla parzystych b wynik zawsze dodatni
		else return a* pow(a*a, b/2); // dla nieparzystych znak wyniku zależny od znaku a
	}
	
	protected Integer mod(Integer a, Integer b) {
		return a % b;
	}
	
	/* ****** operacje przesuwania bitów *****/
	// shift left
	protected Integer shl(Integer a, Integer b) {
		return a << b;
	}
	
	// shift right
	protected Integer shr(Integer a, Integer b) {
		return a >> b;
	}
	
	/* ****** bloki kodu, zmienne lokalne ********/
	// wejście do bloku kodu - umieszczenie nowej tablicy symboli na stosie
	protected void enterBlock() {
		locals.enterScope();
	}
	
	// wyjście z bloku kodu - usunięcie tablicy symboli ze szczytu stosu
	protected void leaveBlock() {
		locals.leaveScope();
	}
	
	// zdefiniowanie nowego symbolu w tablicy symboli będącej na szczycie stosu
	protected void newSymbol(String name) {
		locals.newSymbol(name);
	}
	
	// ustawienie wartości symbolu w pierwszej tablicy od szczytu stosu gdzie symbol został zdefiniowany
	protected void setSymbol(String name, Integer value) {
		locals.setSymbol(name, value);
	}
	
	// pobranie wartości symbolu z pierwszej tablicy od szczytu stosu gdzie symbol został zdefiniowany
	protected Integer getSymbol(String name) {
		return locals.getSymbol(name);
	}
}
