tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import java.util.HashMap;
}

prog    : (expr | print | decl | block)* ;

// akcja dla deklaracji zmiennej, wykorzystanie stosu tablic symboli lokalnych
decl  :
        ^(VAR i1=ID) {newSymbol($i1.text);}
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

// akcja dla reguły wyświetlającej wartość wyrażenia (expr)
print : ^(PRINT e=expr) { drukuj($e.text + "=" + $e.out.toString()); };
    catch [RuntimeException ex] { System.err.println(ex.getMessage()); }


expr returns [Integer out]
	      : ^(SHL e1=expr e2=expr)    { $out = shl($e1.out, $e2.out); } // shift left
	      | ^(SHR e1=expr e2=expr)    { $out = shr($e1.out, $e2.out); } // shift right
	      | ^(PLUS  e1=expr e2=expr)  { $out = add($e1.out, $e2.out); }
        | ^(MINUS e1=expr e2=expr)  { $out = sub($e1.out, $e2.out); }
        | ^(MUL   e1=expr e2=expr)  { $out = mul($e1.out, $e2.out); }
        | ^(DIV   e1=expr e2=expr)  { $out = div($e1.out, $e2.out); }
        | ^(POW   e1=expr e2=expr)  { $out = pow($e1.out, $e2.out); }
        | ^(MODULO e1=expr e2=expr) { $out = mod($e1.out, $e2.out); }
        // przypisanie wartości do zmiennej powinno zwracać tą wartość
        | ^(PODST i1=ID   e1=expr)  { $out = $e1.out; setSymbol($i1.text, $e1.out); }
        | INT                       { $out = getInt($INT.text); }
        // wydobycie wartości zmiennej ze stosu tablic symboli lokalnych
        | i1=ID                     { $out = getSymbol($i1.text); }
        ;
        catch [RuntimeException ex] {
          // sprawdzenie czy wyjątek dotyczy zmiennej
	        if($i1 != null) { 
	          errorID(ex, $i1);
	        } else {
	          System.err.println(ex.getMessage());
	        };
        }

block
  : EB { enterBlock(); } // wejście do bloku kodu
  | LB { leaveBlock(); } // wyjście z bloku kodu
  ;